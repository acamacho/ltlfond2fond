import spot

class LTLGoal():
    def __init__(self,fml):
        fml = fml.replace(' ', '__space__')
        fml_spot = spot.formula(fml)
        vocabulary = list(spot.atomic_prop_collect(fml_spot)) # vocabulary before relabeling
        m = spot.relabeling_map()
        fml_spot = fml_spot.relabel(spot.Pnn,m)
        self.vars_dict = {}
        for newname, oldname in m.items():
            self.vars_dict[oldname.to_str().replace('__space__',' ')] = newname.to_str()

        self.fml = fml_spot.to_str(parenth=True) # parenthesizing for better compatibility
        self.fml = self.fml.replace(' & ', ' && ').replace(' | ', ' || ') # compatibility with bm06
        
        
def tokens2LTLformula(stuff):
    # print(stuff)
    if type(stuff) is str:
        return stuff
    assert(type(stuff) is list)
    if len(stuff) == 0:
        return ""
    if type(stuff) is list:
        is_predicate = True
        for p in stuff:
            if type(p) is not str:
                is_predicate = False
                break
        if is_predicate:
            return "(%s)"  % ' '.join(tokens2LTLformula(p) for p in stuff)
    if stuff[0] == "not":
        assert(len(stuff) == 2)
        return "(!{})".format(tokens2LTLformula(stuff[1]))
    
    elif stuff[0] == "or":
        subformulas = [tokens2LTLformula(p) for p in stuff[1:]]
        return "({})".format('||'.join(tokens2LTLformula(subfml) for subfml in subformulas))

    elif stuff[0] == "and":
        subformulas = [tokens2LTLformula(p) for p in stuff[1:]]
        return "({})".format('&&'.join(tokens2LTLformula(subfml) for subfml in subformulas))
    
    elif stuff[0] == "next":
        assert(len(stuff) == 2)
        return "(X{})".format(tokens2LTLformula(stuff[1]))

    elif stuff[0] == "eventually":
        assert(len(stuff) == 2)
        return "(F{})".format(tokens2LTLformula(stuff[1]))

    elif stuff[0] == "always":
        assert(len(stuff) == 2)
        return "(G{})".format(tokens2LTLformula(stuff[1]))

    elif stuff[0] == "until":
        assert(len(stuff) == 3)
        return "({}U{})".format(tokens2LTLformula(stuff[1]),tokens2LTLformula(stuff[2]))
    
    elif stuff[0] == "release":
        assert(len(stuff) == 3)
        return "({}R{})".format(tokens2LTLformula(stuff[1]),tokens2LTLformula(stuff[2]))

    else:
        # assume it is an AND of all elements in list
        subformulas = [tokens2LTLformula(p) for p in stuff]
        return "({})".format('&&'.join(tokens2LTLformula(subfml) for subfml in subformulas))
        
    print(stuff)
    print(stuff[0])
    raise error
    
# # fml = 'F ( "v1" && "v2" && "v3" && "v4" && "v5" && "v6" && "v7"  )'
# fml = 'G (F ( "v1"  ) )'
# vars_dict = {
#     '(emptyhand)': "v1",
#     '(on b1 b3)': "v2",
#     '(on b2 b1)': "v3",
#     '(on b3 b4)': "v4",
#     '(on-table b4)': "v5",
#     '(on b5 b2)': "v6",
#     '(clear b5)': "v7"
# }