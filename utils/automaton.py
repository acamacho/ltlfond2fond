# LTLFOND2FOND. A tool for translating LTL-FOND planning problems into FOND.
# The MIT License (MIT)

# Copyright (c) 2018, Alberto Camacho <acamacho@cs.toronto.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import spot

class Transition:
    src_id = None
    dst_id = None
    guard = None
    _id = None
    
class Automaton():
    def __init__(self, fml,isLTLf=False):
        self.transitions = None
        
        if not isLTLf:
            fml_spot = spot.formula(fml)
            self.aut = spot.translate(fml_spot, 'BA', 'deterministic', 'high')
        else:
            # isLTLf
            from ext.ltlfkit import ltlf2hoa
            automaton_hoa = ltlf2hoa.ltl2hoa(fml)
            self.aut = spot.automaton(automaton_hoa)
        
    def get_accepting(self):
        Q_Fin = []
        for state in range(0, self.aut.num_states()):
            for transition in self.aut.out(state):
                is_accepting = bool(transition.acc)
                # print(transition.acc)
                if is_accepting:
                    # print("is accepting")
                    # print(transition.cond)
                    # print(transition.src)
                    # print(transition.dst)
                    Q_Fin.append(transition.src)
    
        # print(Q_Fin)
        return set(Q_Fin)
    
    def get_accepting_dest(self):
        Q_Fin = []
        for state in range(0, self.aut.num_states()):
            for transition in self.aut.out(state):
                is_accepting = bool(transition.acc)
                # print(transition.acc)
                if is_accepting:
                    # print("is accepting")
                    # print(transition.cond)
                    # print(transition.src)
                    # print(transition.dst)
                    Q_Fin.append(transition.dst)
    
        # print(Q_Fin)
        return set(Q_Fin)
        
    def get_transitions(self):
        if not self.transitions:
            self.transitions = []
            bdict = self.aut.get_dict()
            idx = 0
            for state in range(0, self.aut.num_states()):
                for transition in self.aut.out(state):
                    src = transition.src
                    dst = transition.dst
                    cnf_lbl = spot.formula(spot.bdd_format_formula(bdict, transition.cond))
                    #cnf_lbl formula is already in DNF
                    # print(" %s   acc sets =%s" % (state,transition.acc))
                    # idx = 0
                    for clause in cnf_lbl.to_str().split(" | "):
                        # print(clause)
                        t = Transition()
                        t.src_id = src
                        t.dst_id = dst
                        t._id = idx
                        idx += 1
                        guard = [ x.replace(" ", "") for x in clause.replace("(","").replace(")","").split("&") if x != "1"]
                        t.guard = guard
                        self.transitions.append(t)
                        # print("%s --%s--> %s" %(src,clause,dst))
        # else
        return self.transitions

    def get_transition(self,idx):
        return self.transitions[idx]
        
    def num_states(self):
        return self.aut.num_states()
    
    def num_transitions(self):
        ntransitions = 0
        for state in range(0, self.aut.num_states()):
            ntransitions += len(self.aut.out(state))
            for transition in self.aut.out(state):
                
                return self.aut.num_states()
    
    def get_init_state_number(self):
        return self.aut.get_init_state_number()
 