# LTLFOND2FOND. A tool for translating LTL-FOND planning problems into FOND.
# The MIT License (MIT)

# Copyright (c) 2018, Alberto Camacho <acamacho@cs.toronto.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
sys.path.append('ext/')
sys.path.append('ext/pddl_parser')

from pddl_parser.PDDL import *

def dump_domain(parser, f=sys.stdout):
    print("(define (domain %s)" % parser.domain_name, file=f)
    print("\t(:requirements %s)" % ' '.join(parser.requirements), file=f)
    print("\t(:constants %s)" % ' '.join(parser.constants), file=f)
    print("\t(:types %s)" % ' '.join(parser.types), file=f)
    print("\t(:predicates %s)" % ' '.join( ["(%s)" % ' '.join(p) for p in parser.predicates]), file=f)
    for act in parser.actions:
        dump_action(act, f)
    print(")", file=f)
 
        
def dump_action(action, f=sys.stdout):
    print("\t(:action %s" % action.name, file=f)
    if len(action.parameters) > 0:
        print("\t\t:parameters (%s)" % ' '.join(action.parameters), file=f)
    pos_preconds = ' '.join( ["(%s)" % ' '.join(p) for p in action.positive_preconditions])
    neg_preconds = ' '.join( ["(not (%s))" % ' '.join(p) for p in action.negative_preconditions])
    print("\t\t:precondition (and %s %s)" % (pos_preconds,neg_preconds), file=f)
    # add_effects = effect2pddl(action.add_effects)
    # del_effects = ' '.join( ["(not (%s))" % ' '.join(p) for p in action.del_effects])
    # cond_effects = effect2pddl(action.cond_effects)
    # print("\t\t:effect (and %s %s %s)" % (add_effects,del_effects,cond_effects), file=f)
    del_effects = [ ['not', p] for p in action.del_effects]
    effects = effect2pddl(action.cond_effects + action.add_effects + del_effects)
    # print(effects)
    print("\t\t:effect %s" % (effects), file=f)
    print("\t)", file=f)


def effect2pddl(stuff):
    # print(stuff)
    if type(stuff) is str:
        return stuff
    assert(type(stuff) is list)
    if len(stuff) == 0:
        return "(and )"
    if type(stuff) is list:
        is_predicate = True
        for p in stuff:
            if type(p) is not str:
                is_predicate = False
                break
        if is_predicate:
            return "(%s)"  % ' '.join(effect2pddl(p) for p in stuff)
    if stuff[0] == "not":
        return "(not %s)" % ' '.join(effect2pddl(p) for p in stuff[1:])
    if stuff[0] == "oneof":
        if len(stuff[1:]) <= 1:
            return effect2pddl(p)
        else:
            return "\n\t\t\t(oneof\n\t\t\t %s)"  % ' '.join(effect2pddl(p) + '\n\t\t\t' for p in stuff[1:])
    elif stuff[0] == "when":
        return "\n\t\t\t(when %s)"  % ' '.join(effect2pddl(p) + '\n\t\t\t' for p in stuff[1:])
    else:
        if len(stuff) > 1:
            if stuff[0] == "and" and len(stuff) > 1:
                return "(and %s)"  % ' '.join(effect2pddl(p) for p in stuff[1:])
            else:
                return "(and %s)"  % ' '.join(effect2pddl(p) for p in stuff)
        else:
            return effect2pddl(p)

def dump_problem(parser, f=sys.stdout):
    print("(define (problem %s)" % parser.problem_name, file=f)
    print("(:domain %s)" % parser.domain_name, file=f)
    print("(:objects %s)" % ' '.join(parser.objects), file=f)
    print("(:init %s)" % ' '.join( effect2pddl(p) for p in parser.state), file=f)
    print("(:goal (dummy_goal))", file=f)
    print(")", file=f)