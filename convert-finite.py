# LTLfFOND2FOND. A tool for translating LTLf-FOND planning problems into FOND.
# The MIT License (MIT)

# Copyright (c) 2018, Alberto Camacho <acamacho@cs.toronto.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
sys.path.append('ext/')
sys.path.append('ext/pddl_parser')

import spot
from pddl_parser.PDDL import *
from utils.automaton import Automaton

def usage():
    return "Usage: python3 convert-finite.py example/domain.pddl example/p01.pddl example/ltl_goal.py"

if len(sys.argv) != 4:
    print(usage())


# STEP 1: parser Domain and Instance files
import sys
import pprint
dom = sys.argv[1]
prob = sys.argv[2]
parser = PDDL_Parser()
parser.parse_domain(dom)
parser.parse_problem(prob)

# STEP 2: Parse LTL formula
from utils import ltlgoal
# STEP 2: Parse LTL formula
from utils import ltlgoal
if len(sys.argv) == 4:
    # the goal formula is given in a separate file
    ltlGoal = ltlgoal.LTLGoal(open(sys.argv[3]).read())
else:
    assert(len(sys.argv) == 3)
    # read the goal formula from PDDL
    parsed_fml = ltlgoal.tokens2LTLformula(parser.positive_goals)
    ltlGoal = ltlgoal.LTLGoal(parsed_fml)
fml = ltlGoal.fml
vars_dict = ltlGoal.vars_dict

# STEP 3: Transform LTL formula into FSA
isLTLf = True
aut_dynamics = Automaton(fml,isLTLf)

# STEP 4: Integrate the dynamics of the automaton within the Domain and Instance
from modules import ltlffond2fond
ltlffond2fond.cross_product(parser,aut_dynamics,vars_dict)

# STEP 5: Dump compiled domain and instance files
from utils.dump import dump_domain, dump_problem

f = open("compiled_domain.pddl","w")
dump_domain(parser, f)
f.close()

f = open("compiled_problem.pddl","w")
dump_problem(parser, f)
f.close()