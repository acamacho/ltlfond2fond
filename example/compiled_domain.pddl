(define (domain blocks-domain)
	(:requirements :non-deterministic :equality :typing)
	(:types block qstate)
	(:predicates (holding ?b - block) (emptyhand) (on-table ?b - block) (on ?b1 ?b2 - block) (clear ?b - block) (autstate ?q - qstate) (world_mode) (sync_mode) (restablish_mode) (can_continue) (dummy_goal))
	(:action pick-up
		:parameters (?b1 ?b2 - block)
		:precondition (and (emptyhand) (clear ?b1) (on ?b1 ?b2) (world_mode) (not (= ?b1 ?b2)))
		:effect (and (and 
			(oneof
			 (and (holding ?b1) (clear ?b2) (not (emptyhand)) (not (clear ?b1)) (not (on ?b1 ?b2)))
			 (and (clear ?b2) (on-table ?b1) (not (on ?b1 ?b2)))
			) (and (and (holding ?b1) (clear ?b2) (not (emptyhand)) (not (clear ?b1)) (not (on ?b1 ?b2))) (and (clear ?b2) (on-table ?b1) (not (on ?b1 ?b2)))) (sync_mode)) (not (world_mode)) (and ))
	)
	(:action pick-up-from-table
		:parameters (?b - block)
		:precondition (and (emptyhand) (clear ?b) (on-table ?b) (world_mode) )
		:effect (and (and 
			(oneof
			 (and )
			 (and (holding ?b) (not (emptyhand)) (not (on-table ?b)))
			) (and (and ) (and (holding ?b) (not (emptyhand)) (not (on-table ?b)))) (sync_mode)) (not (world_mode)) (and ))
	)
	(:action put-on-block
		:parameters (?b1 ?b2 - block)
		:precondition (and (holding ?b1) (clear ?b2) (world_mode) )
		:effect (and (and 
			(oneof
			 (and (on ?b1 ?b2) (emptyhand) (clear ?b1) (not (holding ?b1)) (not (clear ?b2)))
			 (and (on-table ?b1) (emptyhand) (clear ?b1) (not (holding ?b1)))
			) (and (and (on ?b1 ?b2) (emptyhand) (clear ?b1) (not (holding ?b1)) (not (clear ?b2))) (and (on-table ?b1) (emptyhand) (clear ?b1) (not (holding ?b1)))) (sync_mode)) (not (world_mode)) (and ))
	)
	(:action put-down
		:parameters (?b - block)
		:precondition (and (holding ?b) (world_mode) )
		:effect (and (and (on-table ?b) (emptyhand) (clear ?b) (sync_mode)) (not (holding ?b)) (not (world_mode)) (and ))
	)
	(:action pick-tower
		:parameters (?b1 ?b2 ?b3 - block)
		:precondition (and (emptyhand) (on ?b1 ?b2) (on ?b2 ?b3) (world_mode) )
		:effect (and (and 
			(oneof
			 (and )
			 (and (holding ?b2) (clear ?b3) (not (emptyhand)) (not (on ?b2 ?b3)))
			) (and (and ) (and (holding ?b2) (clear ?b3) (not (emptyhand)) (not (on ?b2 ?b3)))) (sync_mode)) (not (world_mode)) (and ))
	)
	(:action put-tower-on-block
		:parameters (?b1 ?b2 ?b3 - block)
		:precondition (and (holding ?b2) (on ?b1 ?b2) (clear ?b3) (world_mode) )
		:effect (and (and 
			(oneof
			 (and (on ?b2 ?b3) (emptyhand) (not (holding ?b2)) (not (clear ?b3)))
			 (and (on-table ?b2) (emptyhand) (not (holding ?b2)))
			) (and (and (on ?b2 ?b3) (emptyhand) (not (holding ?b2)) (not (clear ?b3))) (and (on-table ?b2) (emptyhand) (not (holding ?b2)))) (sync_mode)) (not (world_mode)) (and ))
	)
	(:action put-tower-down
		:parameters (?b1 ?b2 - block)
		:precondition (and (holding ?b2) (on ?b1 ?b2) (world_mode) )
		:effect (and (and (on-table ?b2) (emptyhand) (sync_mode)) (not (holding ?b2)) (not (world_mode)) (and ))
	)
	(:action sync_aut
		:precondition (and (sync_mode) )
		:effect (and (and (restablish_mode)) (not (sync_mode)) (not (autstate q0)) (not (autstate q1)) (not (autstate qc0)) (not (autstate qc1)) (and 
			(when (and (autstate q0) (emptyhand))
			 (and (autstate qnewc0) (can_continue))
			) 
			(when (and (autstate qc0) (emptyhand))
			 (and (autstate qnewc0) (can_continue))
			) 
			(when (and (autstate q0) (not (emptyhand)))
			 (and (autstate qnewc1) (can_continue))
			) 
			(when (and (autstate qc0) (not (emptyhand)))
			 (and (autstate qnewc1) (can_continue))
			) 
			(when (and (autstate q1) (emptyhand))
			 (and (autstate qnewc0) (can_continue))
			) 
			(when (and (autstate qc1) (emptyhand))
			 (and (autstate qnewc0) (can_continue))
			) 
			(when (and (autstate q1) (not (emptyhand)))
			 (and (autstate qnewc1) (can_continue))
			) 
			(when (and (autstate qc1) (not (emptyhand)))
			 (and (autstate qnewc1) (can_continue))
			)))
	)
	(:action restablish_world
		:precondition (and (restablish_mode) )
		:effect (and (and (world_mode)) (not (restablish_mode)) (not (autstate qnew0)) (not (autstate qnew1)) (not (autstate qnewc0)) (not (autstate qnewc1)) (and 
			(when (and (autstate qnew0) (not (autstate qnewc0)))
			 (autstate q0)
			) 
			(when (autstate qnewc0)
			 (not (autstate qc0))
			) 
			(when (and (autstate qnew1) (not (autstate qnewc1)))
			 (autstate q1)
			) 
			(when (autstate qnewc1)
			 (not (autstate qc1))
			)))
	)
	(:action check_and_continue
		:precondition (and (restablish_mode) (can_continue) )
		:effect (and (and 
			(oneof
			 (dummy_goal)
			 (world_mode)
			)) (not (restablish_mode)) (not (can_continue)) (not (autstate qnew0)) (not (autstate qnew1)) (not (autstate qnewc0)) (not (autstate qnewc1)) (and 
			(when (and (autstate qnew0) (not (autstate qnewc0)))
			 (autstate q0)
			) 
			(when (autstate qnewc0)
			 (autstate qc0)
			) 
			(when (and (autstate qnew1) (not (autstate qnewc1)))
			 (autstate q1)
			) 
			(when (autstate qnewc1)
			 (autstate qc1)
			)))
	)
)
