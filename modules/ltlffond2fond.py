# LTLfFOND2FOND. A tool for translating LTLf-FOND planning problems into FOND.
# The MIT License (MIT)

# Copyright (c) 2018, Alberto Camacho <acamacho@cs.toronto.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# import spot
from utils.automaton import *
from pddl_parser.PDDL import Action
import re

at_aut = "prev_autstate ?q - qstate"
is_accepting = "is_accepting ?q - qstate"
dummy_goal = "dummy_goal"

def cross_product(parser,automaton,vars_dict):
    aut_dynamics = AutomatonDynamics(automaton, 0)

    LTL_vars = vars_dict.keys()
    
    update_types(parser,aut_dynamics)
    update_predicates(parser,aut_dynamics)
    update_actions(parser,aut_dynamics,vars_dict)
    update_initial_state(parser,aut_dynamics,vars_dict)
    add_end_action(parser,aut_dynamics,vars_dict)

def update_types(parser,aut_dynamics):
    parser.types += [ "qstate" ]
    
def update_predicates(parser,aut_dynamics):
    parser.predicates += [ at_aut.split()]
    parser.predicates += [ is_accepting.split()]
    parser.predicates += [[dummy_goal]]
    
def add_end_action(parser,aut_dynamics,vars_dict):
    name = "end"
    parameters = [autstate_param('')]
    positive_preconditions = [scan_tokens_from_string(autstate_param_fluent('')), scan_tokens_from_string(is_accepting_param_fluent(''))]
    negative_preconditions = []
    add_effects = [[dummy_goal]]
    del_effects = []
    cond_effects = []
    action = Action(name, parameters, positive_preconditions, negative_preconditions, add_effects, del_effects, cond_effects)
    parser.actions.append(action)
    
def update_initial_state(parser,aut_dynamics,vars_dict):
    
    # parser.objects
    aut_states = range(0, aut_dynamics.aut.num_states())
    parser.objects += [autstate_object(state_id) for state_id in aut_states]
    
    # parser.state
    init_state_id = aut_dynamics.aut.get_init_state_number()
    
    parser.state += [scan_tokens_from_string(autstate_fluent(init_state_id))]
    
    Q_Fin = aut_dynamics.aut.get_accepting_dest()
    parser.state += [scan_tokens_from_string(is_accepting_fluent(q_id)) for q_id in Q_Fin ]
    
    parser.positive_goals = [[dummy_goal]]
    parser.negative_goals = []
    
def update_actions(parser,aut_dynamics,vars_dict):
    for action in parser.actions:
        transitions = aut_dynamics.aut.get_transitions()
        # Q_Fin = aut_dynamics.aut.get_accepting_dest()
        
        add_effects = []
        del_effects = []
        cond_effects = []
        
        for t in transitions:
            positive_guard_fluents,negative_guard_fluents = positive_and_negative_guard_fluents(t.guard,vars_dict)

            condition = [autstate_fluent(t.src_id)]
            condition += guard_to_fluents(t.guard,vars_dict)

            # This is only complete when the automaton is a DFA
            # TODO: make it work for NFAs. Look at Jorge Baier's Thesis
            effect = []
            if t.src_id != t.dst_id:
                effect.append('(not %s)' % autstate_fluent(t.src_id))
                effect.append(autstate_fluent(t.dst_id))

            cond_effect_string = "(when (%s) (%s))" % (' '.join(condition), ' '.join(effect))
            cond_effect_tokens = scan_tokens_from_string(cond_effect_string)
            cond_effects.append(cond_effect_tokens)
        
        aut_states = range(0, aut_dynamics.aut.num_states())

        # update action
        action.add_effects += add_effects
        action.del_effects += del_effects
        action.cond_effects += cond_effects
        


def to_str(fluent_list):
    return "(%s)" % ' '.join(fluent_list)


    
def scan_tokens_from_string(string):
        str = re.sub(r';.*$', '', string, flags=re.MULTILINE).lower()
        # Tokenize
        stack = []
        list = []
        for t in re.findall(r'[()]|[^\s()]+', str):
            if t == '(':
                stack.append(list)
                list = []
            elif t == ')':
                if stack:
                    l = list
                    list = stack.pop()
                    list.append(l)
                else:
                    raise Exception('Missing open parentheses')
            else:
                list.append(t)
        if stack:
            raise Exception('Missing close parentheses')
        if len(list) != 1:
            raise Exception('Malformed expression')
        return list[0]
    
def guard_to_fluents(guard,vars_dict):
    fluents = []
    for v in guard:
        if '!' in v:
            target = v[1:]
        else:
            target = v
        for key in vars_dict.keys():
            if vars_dict[key] == target:
                if '!' in v:
                    fluents.append('(not (%s))' % key)
                else:
                    fluents.append('(%s)' % key)
    return fluents
    
def positive_and_negative_guard_fluents(guard,vars_dict):
    positive_fluents = []
    negative_fluents = []
    for v in guard:
        if '!' in v:
            target = v[1:]
        else:
            target = v
        for key in vars_dict.keys():
            if vars_dict[key] == target:
                if '!' in v:
                    negative_fluents.append('(%s)' % key)
                else:
                    positive_fluents.append('(%s)' % key)
    return positive_fluents,negative_fluents

def does_intersect(list1,list2):
    return bool(set(list1) & set(list2))
    
def autstate_fluent(q_id):
    return '(prev_autstate q%s)' % q_id

def autstate_param_fluent(q_id):
    return '(prev_autstate ?q%s)' % q_id

def is_accepting_fluent(q_id):
    return '(is_accepting q%s)' % q_id

def is_accepting_param_fluent(q_id):
    return '(is_accepting ?q%s)' % q_id

def autstate_object(q_id):
    return 'q%s - qstate' % q_id

def autstate_param(q_id):
    return '?q%s - qstate' % q_id


class AutomatonDynamics():
    def __init__(self, aut, aut_id):
        self.aut = aut
        self.aut_id = aut_id
        
    # def q(self,k):
    #     return "aut_%s_q_%s" % (self.aut_id, k)

    # def t(self,t_id):
    #     return "aut_%s_t_%s" % (self.aut_id, t_id)
