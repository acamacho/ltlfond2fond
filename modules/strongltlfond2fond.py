# LTLFOND2FOND. A tool for translating LTL-FOND planning problems into FOND.
# The MIT License (MIT)

# Copyright (c) 2018, Alberto Camacho <acamacho@cs.toronto.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import spot
from utils.automaton import *
from pddl_parser.PDDL import Action
import re

# at_aut = "autstate ?q - qstate"
old_aut = "oldautstate ?q - qstate"
new_aut = "newautstate ?q - qstate"
def oldCnt(k): return "oldcnt_idx{} ?q - qstate".format(k)
def newCnt(k): return "newcnt_idx{} ?q - qstate".format(k)
world_mode = "world_mode"
trans_mode = "trans_mode"
sync_mode = "sync_mode"
dummy_goal = "dummy_goal"

def cross_product(parser,automaton,vars_dict,coBuchi_bound):
    aut_dynamics = AutomatonDynamics(automaton, 0)

    LTL_vars = vars_dict.keys()
    mutexes = compute_mutexes([vars_dict[fluent] for fluent in LTL_vars], [aut_dynamics])

    update_types(parser,aut_dynamics)
    update_predicates(parser,aut_dynamics,coBuchi_bound)
    update_actions(parser,aut_dynamics,vars_dict,mutexes)
    add_trans_actions(parser,aut_dynamics,vars_dict,mutexes,coBuchi_bound)
    add_start_sync_action(parser,aut_dynamics,vars_dict,mutexes,coBuchi_bound)
    add_sync_actions(parser,aut_dynamics,vars_dict,mutexes,coBuchi_bound)
    add_continue_action(parser,aut_dynamics,vars_dict,mutexes)
    update_initial_state(parser,aut_dynamics,vars_dict,mutexes)

def update_types(parser,aut_dynamics):
    parser.types += [ "qstate" ]
    
def update_predicates(parser,aut_dynamics,coBuchi_bound):
    parser.predicates += [ old_aut.split() ]
    parser.predicates += [ new_aut.split() ]
    parser.predicates += [ oldCnt(k).split() for k in range(coBuchi_bound) ]
    parser.predicates += [ newCnt(k).split() for k in range(coBuchi_bound+1) ]
    parser.predicates += [[world_mode]]
    parser.predicates += [[trans_mode]]
    parser.predicates += [[sync_mode]]
    parser.predicates += [[dummy_goal]]
    
def update_initial_state(parser,aut_dynamics,vars_dict,mutexes):
    
    # parser.objects
    aut_states = range(0, aut_dynamics.aut.num_states())
    parser.objects += [autstate_object(state_id) for state_id in aut_states]
    
    # parser.state
    init_state_id = aut_dynamics.aut.get_init_state_number()
    
    parser.state += [[trans_mode], scan_tokens_from_string(autstate_fluent_object(init_state_id)), scan_tokens_from_string(autcounter_fluent_object(init_state_id,0))]
    
    parser.positive_goals = [[dummy_goal]]
    parser.negative_goals = []
    
def update_actions(parser,aut_dynamics,vars_dict,mutexes):
    for action in parser.actions:
        action.positive_preconditions += [[world_mode]]
        action.del_effects += [[world_mode]]
        action.add_effects += [[trans_mode]]

def add_trans_actions(parser,aut_dynamics,vars_dict,mutexes,coBuchi_bound):
    Q_Fin = aut_dynamics.aut.get_accepting_dest()
    
    aut_states = range(0, aut_dynamics.aut.num_states())
    
    for q_old in aut_states:
        for idx_old in range(coBuchi_bound):
            name = "trans_aut_q{}_idx{}".format(q_old, idx_old)
            parameters = []
            positive_preconditions = [[trans_mode], scan_tokens_from_string(autstate_fluent_object(q_old)), scan_tokens_from_string(autcounter_fluent_object(q_old,idx_old))]
            negative_preconditions = []
            add_effects = []
            del_effects = [ scan_tokens_from_string(autstate_fluent_object(q_old)) ]
            cond_effects = []
            transitions = aut_dynamics.aut.get_transitions()
            for t in transitions:
                if t.src_id != q_old:
                    continue
                condition = guard_to_fluents(t.guard,vars_dict)
                
                if len(condition) == 0: #true condition
                    effect = [ scan_tokens_from_string(autstate_fluent_object_new(t.dst_id)), scan_tokens_from_string(autcounter_fluent_object_new(t.dst_id,idx_old + int(t.dst_id in Q_Fin)))]
                    add_effects.append( effect )
                    continue
                
                effect = [autstate_fluent_object_new(t.dst_id)] +  [autcounter_fluent_object_new(t.dst_id,idx_old + int(t.dst_id in Q_Fin))]
                cond_effect_string = "(when (%s) (%s))" % (' '.join(condition), ' '.join(effect))
                cond_effect_tokens = scan_tokens_from_string(cond_effect_string)
                cond_effects.append(cond_effect_tokens)
                
            action = Action(name, parameters, positive_preconditions, negative_preconditions, add_effects, del_effects, cond_effects)
            parser.actions.append(action)

def add_start_sync_action(parser,aut_dynamics,vars_dict,mutexes,coBuchi_bound):
    name = "start_sync"
    parameters = []
    positive_preconditions = [[trans_mode]]
    negative_preconditions = [scan_tokens_from_string(autstate_fluent_object(q_id)) for q_id in range(aut_dynamics.aut.num_states()) ]
    
    add_effects = [[sync_mode]]
    del_effects = [[trans_mode]]
    for q_id in range(aut_dynamics.aut.num_states()):
        for m in range(coBuchi_bound):
            del_effects.append( [ scan_tokens_from_string(autcounter_fluent_object(q_id,m)) ] )
    cond_effects = []
    action = Action(name, parameters, positive_preconditions, negative_preconditions, add_effects, del_effects, cond_effects)
    parser.actions.append(action)


def add_sync_actions(parser,aut_dynamics,vars_dict,mutexes,coBuchi_bound):
    
    for m in range(coBuchi_bound):
        name = "sync_aut_idx{}".format(m)
        q_id = ''
        parameters = [autstate_variable(q_id)]
        positive_preconditions = [[sync_mode], scan_tokens_from_string(autstate_fluent_new(q_id)), scan_tokens_from_string(autcounter_fluent_new(q_id,m))]
        negative_preconditions = [scan_tokens_from_string(autcounter_fluent_new(q_id,n)) for n in range(m+1,coBuchi_bound+1)]
        cond_effects = []
        add_effects = [scan_tokens_from_string(autstate_fluent(q_id)), scan_tokens_from_string(autcounter_fluent(q_id,m))]
        del_effects = [scan_tokens_from_string(autstate_fluent_new(q_id))] + [scan_tokens_from_string(autcounter_fluent_new(q_id,n)) for n in range(coBuchi_bound+1)]

        action = Action(name, parameters, positive_preconditions, negative_preconditions, add_effects, del_effects, cond_effects)
        parser.actions.append(action)
    
def scan_tokens_from_string(string):
        str = re.sub(r';.*$', '', string, flags=re.MULTILINE).lower()
        # Tokenize
        stack = []
        list = []
        for t in re.findall(r'[()]|[^\s()]+', str):
            if t == '(':
                stack.append(list)
                list = []
            elif t == ')':
                if stack:
                    l = list
                    list = stack.pop()
                    list.append(l)
                else:
                    raise Exception('Missing open parentheses')
            else:
                list.append(t)
        if stack:
            raise Exception('Missing close parentheses')
        if len(list) != 1:
            raise Exception('Malformed expression')
        return list[0]
    
def guard_to_fluents(guard,vars_dict):
    fluents = []
    for v in guard:
        if '!' in v:
            target = v[1:]
        else:
            target = v
        for key in vars_dict.keys():
            if vars_dict[key] == target:
                if '!' in v:
                    fluents.append('(not (%s))' % key)
                else:
                    fluents.append('(%s)' % key)
    return fluents
    
def autstate_fluent(q_id):
    return '(oldAutstate ?q%s)' % q_id

def autstate_fluent_new(q_id):
    return '(newAutstate ?q%s)' % q_id

def autcounter_fluent(q_id, idx_id):
    return '(oldCnt_idx{} ?q{})'.format(idx_id,q_id)

def autcounter_fluent_new(q_id, idx_id):
    return '(newCnt_idx{} ?q{})'.format(idx_id,q_id)

def autcounter_fluent_object(q_id, idx_id):
    return '(oldCnt_idx{} q{})'.format(idx_id,q_id)

def autcounter_fluent_object_new(q_id, idx_id):
    return '(newCnt_idx{} q{})'.format(idx_id,q_id)

def autstate_fluent_object(q_id):
    return '(oldAutstate q%s)' % q_id

def autstate_fluent_object_new(q_id):
    return '(newAutstate q%s)' % q_id

def autstate_object(q_id):
    return 'q%s - qstate' % q_id

def autstate_variable(q_id):
    return '?q%s - qstate' % q_id
    
def add_continue_action(parser,aut_dynamics,vars_dict,mutexes):
    name = "continue"
    parameters = []
    positive_preconditions = [[sync_mode]]
    negative_preconditions = [scan_tokens_from_string(autstate_fluent_object_new(q)) for q in range(aut_dynamics.aut.num_states()) ]
    
    add_effects = [['oneof', [dummy_goal], [world_mode]]]
    del_effects = [[sync_mode]]
    cond_effects = []

    action = Action(name, parameters, positive_preconditions, negative_preconditions, add_effects, del_effects, cond_effects)
    parser.actions.append(action)
    
def compute_mutexes(variables, automata):
    # returns a matrix M where M[l] is the set of automata transitions
    # that are mutex with a literal l in Literals(variables)
    mutexes = {}
    for v in variables:
        # print("Testing var %s" % v )
        mutexes[v] = []
        mutexes['!'+ v] = []
        for aut_dynamics in automata:
            mutexes[v] += get_mutex_transitions_for_literal(aut_dynamics, v)
            mutexes['!'+ v] += get_mutex_transitions_for_literal(aut_dynamics, '!' + v)
    return mutexes

def get_mutex_transitions_for_literal(aut_dynamics, literal):
    # returns the set of transitions in the given automaton 
    # that are mutex with the given literal
    if '!' in literal:
        neg_literal = literal[1:]
    else:
        neg_literal = '!' + literal
    mutexes = []
    transitions = aut_dynamics.aut.get_transitions()
    for t in transitions:
        if neg_literal in t.guard:
            mutexes.append(aut_dynamics.t(t._id))
    return set(mutexes)
    
    

class AutomatonDynamics():
    def __init__(self, aut, aut_id):
        self.aut = aut
        self.aut_id = aut_id
        
    def q(self,k):
        return "aut_%s_q_%s" % (self.aut_id, k)

    def t(self,t_id):
        return "aut_%s_t_%s" % (self.aut_id, t_id)
