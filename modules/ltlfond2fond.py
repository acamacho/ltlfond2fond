# LTLFOND2FOND. A tool for translating LTL-FOND planning problems into FOND.
# The MIT License (MIT)

# Copyright (c) 2018, Alberto Camacho <acamacho@cs.toronto.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import spot
from utils.automaton import *
from pddl_parser.PDDL import Action
import re

# at_aut = "at_aut ?q - autstate"
# at_aut_token = "autstate ?q - qstate"
at_aut = "autstate ?q - qstate"
world_mode = "world_mode"
sync_mode = "sync_mode"
reestablish_mode = "reestablish_mode"
# poss_t = "poss ?t - transition"
can_continue = "can_continue"
dummy_goal = "dummy_goal"

def cross_product(parser,automaton,vars_dict):
    aut_dynamics = AutomatonDynamics(automaton, 0)

    LTL_vars = vars_dict.keys()
    mutexes = compute_mutexes([vars_dict[fluent] for fluent in LTL_vars], [aut_dynamics])
    # for fluent in LTL_vars:
    #     print(mutexes[vars_dict[fluent]])

    update_types(parser,aut_dynamics)
    update_predicates(parser,aut_dynamics)
    update_actions(parser,aut_dynamics,vars_dict,mutexes)
    add_sync_action(parser,aut_dynamics,vars_dict,mutexes)
    add_world_action(parser,aut_dynamics,vars_dict,mutexes)
    add_continue_action(parser,aut_dynamics,vars_dict,mutexes)
    update_initial_state(parser,aut_dynamics,vars_dict,mutexes)

def update_types(parser,aut_dynamics):
    parser.types += [ "qstate" ]
    
def update_predicates(parser,aut_dynamics):
    parser.predicates += [ at_aut.split()]
    # parser.predicates += [ at_aut_token.split()]
    # parser.predicates += [ poss_t.split()]
    parser.predicates += [[world_mode]]
    parser.predicates += [[sync_mode]]
    parser.predicates += [[reestablish_mode]]
    parser.predicates += [[can_continue]]
    parser.predicates += [[dummy_goal]]
    
def update_initial_state(parser,aut_dynamics,vars_dict,mutexes):
    
    # parser.objects
    aut_states = range(0, aut_dynamics.aut.num_states())
    parser.objects += [autstate_object(state_id) for state_id in aut_states]
    parser.objects += [checked_autstate_object(state_id) for state_id in aut_states]
    parser.objects += [autstate_object_new(state_id) for state_id in aut_states]
    parser.objects += [checked_autstate_object_new(state_id) for state_id in aut_states]

    
    # parser.state
    init_state_id = aut_dynamics.aut.get_init_state_number()
    
    parser.state += [[sync_mode], scan_tokens_from_string(autstate_fluent(init_state_id))]
    
    parser.positive_goals = [[dummy_goal]]
    parser.negative_goals = []
    
def update_actions(parser,aut_dynamics,vars_dict,mutexes):
    for action in parser.actions:
        action.positive_preconditions += [[world_mode]]
        
        disabled_transitions = []
        # for effect in action.add_effects:
        #     for fluent in effect:
        #         print(fluent)
        #         disabled_transitions += mutexes[ vars_dict[to_str(fluent)] ]
        # for effect in action.del_effects:
        #     for fluent in effect:
        #         disabled_transitions += mutexes[ '!' + vars_dict[fluent] ]
        
        action.add_effects += [[sync_mode]]
        action.del_effects += [[world_mode]]
        
        # print(action)

def to_str(fluent_list):
    return "(%s)" % ' '.join(fluent_list)

def add_sync_action(parser,aut_dynamics,vars_dict,mutexes):
    name = "sync_aut"
    parameters = []
    positive_preconditions = [[sync_mode]]
    negative_preconditions = []
    cond_effects = []

    transitions = aut_dynamics.aut.get_transitions()
    # Q_Fin = aut_dynamics.aut.get_accepting_dest()
    Q_Fin = aut_dynamics.aut.get_accepting()
    
    for t in transitions:
        condition = [autstate_fluent(t.src_id)] + guard_to_fluents(t.guard,vars_dict)
        if t.dst_id in Q_Fin:
            effect = [checked_autstate_fluent_new(t.dst_id), "(%s)" % can_continue]
        else:
            effect = [autstate_fluent_new(t.dst_id)]
        cond_effect_string = "(when (%s) (%s))" % (' '.join(condition), ' '.join(effect))
        cond_effect_tokens = scan_tokens_from_string(cond_effect_string)
        # print(cond_effect_tokens)
        cond_effects.append(cond_effect_tokens)
        
        
        condition = [checked_autstate_fluent(t.src_id)] + guard_to_fluents(t.guard,vars_dict)
        if t.dst_id in Q_Fin:
            effect = [checked_autstate_fluent_new(t.dst_id), "(%s)" % can_continue]
        else:
            effect = [checked_autstate_fluent_new(t.dst_id)]
        cond_effect_string = "(when (%s) (%s))" % (' '.join(condition), ' '.join(effect))
        cond_effect_tokens = scan_tokens_from_string(cond_effect_string)
        # print(cond_effect_tokens)
        cond_effects.append(cond_effect_tokens)
    
    add_effects = [[reestablish_mode]]
    del_effects = [[sync_mode]]
    
    aut_states = range(0, aut_dynamics.aut.num_states())
    del_effects += [scan_tokens_from_string(autstate_fluent(state_id)) for state_id in aut_states]
    del_effects += [scan_tokens_from_string(checked_autstate_fluent(state_id)) for state_id in aut_states]

    action = Action(name, parameters, positive_preconditions, negative_preconditions, add_effects, del_effects, cond_effects)
    parser.actions.append(action)
    # print(action)
    
    
def scan_tokens_from_string(string):
        str = re.sub(r';.*$', '', string, flags=re.MULTILINE).lower()
        # Tokenize
        stack = []
        list = []
        for t in re.findall(r'[()]|[^\s()]+', str):
            if t == '(':
                stack.append(list)
                list = []
            elif t == ')':
                if stack:
                    l = list
                    list = stack.pop()
                    list.append(l)
                else:
                    raise Exception('Missing open parentheses')
            else:
                list.append(t)
        if stack:
            raise Exception('Missing close parentheses')
        if len(list) != 1:
            raise Exception('Malformed expression')
        return list[0]
    
def guard_to_fluents(guard,vars_dict):
    fluents = []
    for v in guard:
        if '!' in v:
            target = v[1:]
        else:
            target = v
        for key in vars_dict.keys():
            if vars_dict[key] == target:
                if '!' in v:
                    fluents.append('(not %s)' % key)
                else:
                    fluents.append('(%s)' % key)
    return fluents
    
def autstate_fluent(q_id):
    return '(autstate q%s)' % q_id

def autstate_fluent_new(q_id):
    return '(autstate qNew%s)' % q_id

def checked_autstate_fluent(q_id):
    return '(autstate qc%s)' % q_id

def checked_autstate_fluent_new(q_id):
    return '(autstate qNewc%s)' % q_id

# 
def autstate_object(q_id):
    return 'q%s - qstate' % q_id

def autstate_object_new(q_id):
    return 'qNew%s - qstate' % q_id

def checked_autstate_object(q_id):
    return 'qc%s - qstate' % q_id

def checked_autstate_object_new(q_id):
    return 'qNewc%s - qstate' % q_id


def add_world_action(parser,aut_dynamics,vars_dict,mutexes):
    name = "reestablish_world"
    parameters = []
    positive_preconditions = [[reestablish_mode]]
    negative_preconditions = []
    cond_effects = []
    
    aut_states = range(0, aut_dynamics.aut.num_states())
    for state_id in aut_states:
        cond_effect_string = "(when (%s (not %s)) %s)" % (autstate_fluent_new(state_id), checked_autstate_fluent_new(state_id), autstate_fluent(state_id))
        cond_effect_tokens = scan_tokens_from_string(cond_effect_string)
        cond_effects.append(cond_effect_tokens)
        
        condition = [checked_autstate_fluent_new(state_id)]
        effect = [checked_autstate_fluent(state_id)]
        cond_effect_string = "(when %s  %s)" % (' '.join(condition), ' '.join(effect))
        cond_effect_tokens = scan_tokens_from_string(cond_effect_string)
        cond_effects.append(cond_effect_tokens)
    
    add_effects = [[world_mode]]
    del_effects = [[reestablish_mode]]

    aut_states = range(0, aut_dynamics.aut.num_states())
    del_effects += [scan_tokens_from_string(autstate_fluent_new(state_id)) for state_id in aut_states]
    del_effects += [scan_tokens_from_string(checked_autstate_fluent_new(state_id)) for state_id in aut_states]


    action = Action(name, parameters, positive_preconditions, negative_preconditions, add_effects, del_effects, cond_effects)
    parser.actions.append(action)
    # print(action)
    
def add_continue_action(parser,aut_dynamics,vars_dict,mutexes):
    name = "check_and_continue"
    parameters = []
    positive_preconditions = [[reestablish_mode], [can_continue]]
    negative_preconditions = []
    cond_effects = []
    
    aut_states = range(0, aut_dynamics.aut.num_states())
    for state_id in aut_states:
        cond_effect_string = "(when (%s (not %s)) (not %s))" % (autstate_fluent_new(state_id), checked_autstate_fluent_new(state_id), autstate_fluent(state_id))
        cond_effect_tokens = scan_tokens_from_string(cond_effect_string)
        cond_effects.append(cond_effect_tokens)
        
        cond_effect_string = "(when %s %s)" % (checked_autstate_fluent_new(state_id), checked_autstate_fluent(state_id))
        cond_effect_tokens = scan_tokens_from_string(cond_effect_string)
        cond_effects.append(cond_effect_tokens)
    
    del_effects = [[reestablish_mode], [can_continue]]
    aut_states = range(0, aut_dynamics.aut.num_states())
    del_effects += [scan_tokens_from_string(autstate_fluent_new(state_id)) for state_id in aut_states]
    del_effects += [scan_tokens_from_string(checked_autstate_fluent_new(state_id)) for state_id in aut_states]


    add_effects = [['oneof', [dummy_goal], [world_mode]]]
    action = Action(name, parameters, positive_preconditions, negative_preconditions, add_effects, del_effects, cond_effects)
    parser.actions.append(action)
    # print(action)
    
def compute_mutexes(variables, automata):
    # returns a matrix M where M[l] is the set of automata transitions
    # that are mutex with a literal l in Literals(variables)
    mutexes = {}
    for v in variables:
        # print("Testing var %s" % v )
        mutexes[v] = []
        mutexes['!'+ v] = []
        for aut_dynamics in automata:
            mutexes[v] += get_mutex_transitions_for_literal(aut_dynamics, v)
            mutexes['!'+ v] += get_mutex_transitions_for_literal(aut_dynamics, '!' + v)
    return mutexes

def get_mutex_transitions_for_literal(aut_dynamics, literal):
    # returns the set of transitions in the given automaton 
    # that are mutex with the given literal
    if '!' in literal:
        neg_literal = literal[1:]
    else:
        neg_literal = '!' + literal
    mutexes = []
    transitions = aut_dynamics.aut.get_transitions()
    for t in transitions:
        if neg_literal in t.guard:
            mutexes.append(aut_dynamics.t(t._id))
    return set(mutexes)
    
    

class AutomatonDynamics():
    def __init__(self, aut, aut_id):
        self.aut = aut
        self.aut_id = aut_id
        
    def q(self,k):
        return "aut_%s_q_%s" % (self.aut_id, k)

    def t(self,t_id):
        return "aut_%s_t_%s" % (self.aut_id, t_id)
