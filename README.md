LTLFOND2FOND: A translator from LTL-FOND to FOND planning problems.

What can we Solve?
==================
This tool can be used to find solutions to LTL FOND and LTLf FOND planning problems via compilation to FOND planning with final-state goals. In particular:

* Strong-cyclic solutions to LTL FOND: via compilation to strong-cyclic FOND planning.
* Strong-cyclic solutions to LTLf FOND: via compilation to strong-cyclic FOND planning.
* Strong solutions to LTL FOND: via compilation to strong-cyclic FOND planning.
* Strong solutions to LTLf FOND: via compilation to strong FOND planning.

Usage
=====
The input to the tool is a PDDL FOND domain and instance files, with an LTL (resp. LTLf) goal. See the example in the '/example' folder.
The tool produces a final-state goal FOND domain and instance files that can be solved with an off-the-shelf FOND planner. We recommend FOND planner myND.

To convert strong-cyclic LTL-FOND to strong-cyclic FOND:
    
    python3 convert.py example/domain.pddl example/p01.pddl example/goal.ltl
    
To convert strong LTL-FOND to strong-cyclic FOND:
    
    python3 strong-convert.py example/domain.pddl example/p01.pddl example/goal.ltl


To convert strong (resp. strong-cyclic) LTLf-FOND (aka Finite LTL-FOND) to strong (resp. strong-cyclic) FOND:
    
    python3 convert-finite.py example/domain.pddl example/p01.pddl example/goal.ltl

The translation follows the compilation presented in [1] available [here]:

[1]: Alberto Camacho, Eleni Triantafillou, Christian J. Muise, Jorge A. Baier, Sheila A. McIlraith. Non-Deterministic Planning with Temporally Extended Goals: LTL over Finite and Infinite Traces. In Proceedings of the Thirty-First AAAI Conference on Artificial Intelligence (AAAI), 2017, pages 3716-3724.

[here]: http://www.cs.toronto.edu/~sheila/publications/cam-etal-aaai17.pdf

Citing
======
If you use this code and want to refer to the LTLFOND2FOND compilation, please cite these papers appropriately:

If you do strong-cyclic LTL FOND or strong-cyclic LTLf FOND planning, then cite this paper:
    
    @inproceedings{cam-tri-bai-mui-mci-aaai17,
      author    = {Alberto Camacho and
                   Eleni Triantafillou and
                   Christian J. Muise and
                   Jorge A. Baier and
                   Sheila A. McIlraith},
      title     = {Non-Deterministic Planning with Temporally Extended Goals: {LTL} over Finite and Infinite Traces},
      booktitle = {Proceedings of the Thirty-First AAAI Conference on Artificial Intelligence ({AAAI})},
      url_paper = {http://www.cs.toronto.edu/~sheila/publications/cam-etal-aaai17.pdf},
      pages     = {3716-3724},
      year      = {2017}
    }
    
If you do strong LTL FOND or strong LTLf FOND planning, then cite this paper:
    
    @inproceedings{cam-mci-ijcai19,
      author    = {Alberto Camacho and
                   Sheila A. McIlraith},
      title     = {Strong Fully Observable Non-Deterministic Planning with LTL and LTL-f Goals},
      booktitle = {Proceedings of the Twenty-Eighth International Joint Conference on Artificial Intelligence ({IJCAI})},
      url_paper = {https://www.ijcai.org/proceedings/2019/0767.pdf},
      pages     = {5523--5531},
      year      = {2019}
    }
    

Dependencies
============
The tool requires python3 and spot.


LTLfKit: needed in the LTLf-FOND planning compilations. The tool is used to transform LTLf into finite-state automata. 
LTLfKit is included as a submodule. Follow installation instructions in ext/ltlfkit/INSTALL.md.

    git submodule update --init

Spot can be installed from the link below. We recommend installation from source files.

    https://spot.lrde.epita.fr/install.html

Legacy code
===========

The parser builds on the code in:

* Mau Magnaguagno, & Felipe Meneguzzi. (2017, June 5). pucrs-automated-planning/pddl-parser: Beta release 1 (Version beta1). Zenodo. http://doi.org/10.5281/zenodo.802957